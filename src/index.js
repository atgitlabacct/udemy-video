import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import YTSearch from 'youtube-api-search';
import SearchBar from './components/search_bar';
import VideoList from './components/video_list';
import VideoDetail from './components/video_detail';

const API_KEY = 'AIzaSyBkTJTQZ4gdMlUyvpgRcJMqsgH-OgMUeDw'

class App extends Component {
    constructor(props) {
        super(props);

        // Set the initial state.
        // Do not use state except in constructor
        this.state = {
            videos: [],
            selected_video: null
        };

        this.videoSearch('surfboards')
    }

    videoSearch(term) {
        YTSearch({key: API_KEY, term: term}, (videos) => {
            this.setState({
                videos: videos,
                selectedVideo: videos[0]
            });

        });

    }

    render() {
        const videoSearch =_.debounce((term) => { this.videoSearch(term) }, 300)

        var html = (
            <div>
                <SearchBar onSearchTermChange={term => videoSearch(term)}/>
                <VideoDetail video={this.state.selectedVideo} />
                <VideoList
                    onVideoSelect={ (selectedVideo) => this.setState({selectedVideo}) }
                    videos={this.state.videos} />
            </div>
        );

        return html;
    }
}

ReactDOM.render(<App />, document.querySelector('.container'));
