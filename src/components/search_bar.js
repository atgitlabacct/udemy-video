import _ from 'lodash'
import React, { Component } from 'react';

class SearchBar extends Component {
    constructor(props) {
        super(props);

        /*
         * Only in the constructor will we set the state
         * like below, every where else we use setState
         */
        this.state = { term: 'Starting value' };
    }

    render() {
        return (
            <div className="search-bar">
                <input
                    value={this.state.term}
                    onChange={(event) => this.onInputChange(event.target.value)} />
            </div>
        );
    }

    onInputChange(term) {
        this.setState({term: term})
        this.props.onSearchTermChange(term)
    }
}

export default SearchBar;
